'''
Flask

Django
Django-Rest-Framework

ORM: Object Relation Mapping
  sqlalchemy

http://localhost:5000/ruta
http://localhost:5000/


http   80
https  443

loopback:
  localhost
  127.0.0.1

             request
             ------->
[Cliente]              [Servidor]
             <-------
             response

'''

from flask import Flask, Response, request
import json

app = Flask(__name__)
app.debug = True


@app.route('/')
def raiz_sitio():
    return 'Este es el inicio!'

@app.route('/ruta')
def ruta():
    return 'Consultando la Ruta'

@app.route('/ip')
def consultar_ip():
    print(request.args)
    print(request.remote_addr)
    return 'Su IP es ' + request.remote_addr

@app.route('/parametros/<arg1>')
def parametros(arg1):
    print(arg1)
    return 'Desde parametros: ' + arg1


# Los endpoints para administrar productos

productos = {
  'frutas': {1: 'Manzana', 2: 'Pera', 3: 'Mango'},
  'verduras': {1: 'Papa', 2: 'Acelga', 3: 'Lechuga'},
}



@app.route('/productos/')
def listar_producto():
    datos = json.dumps(productos)
    return Response(datos, mimetype='application/json')

@app.route('/productos/<familia>/<id>')
def leer_producto(familia, id):
    id = int(id)

    if familia in productos:
        if id in productos[familia]:
            producto = productos[familia][id]

            return Response(producto, status=200)
    
    return Response(status=404)

@app.route('/productos/', methods=['POST'])       # Decorador   (wrapper o envolvedor)
def ingresar_producto():
    if not 'id' in request.form:
        return Response('Falta id', status=400)

    if not 'nombre' in request.form:
        return Response('Falta nombre', status=400)
    
    if not 'familia' in request.form:
        return Response('Falta familia', status=400)

    id = request.form['id']
    nombre = request.form['nombre']
    familia = request.form['familia']

    if not familia in productos:
        productos[familia] = {}

    productos[familia][id] = nombre

    return Response(status=201)



app.run()



'''
https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types

Postman

'''

