import numpy as np

ran = np.random.default_rng(0)

a = (ran.random((2, 3)) * 10).astype('int32')
b = (ran.random((2, 4)) * 10).astype('int32')
c = (ran.random((2, 2)) * 10).astype('int32')

print(a)
print()
print(b)
print()
print(c)
print()

h = np.hstack((a, b, c))
print(h)

print('-' * 50)

a = (ran.random((3, 3)) * 10).astype('int32')
b = (ran.random((4, 3)) * 10).astype('int32')
c = (ran.random((2, 3)) * 10).astype('int32')

print(a)
print()
print(b)
print()
print(c)
print()

v = np.vstack((a, b, c))
print(v)

print('-' * 50)
print(h.shape)
s1 = np.hsplit(h, 3)
print(s1[0])
print(s1[1])
print(s1[2])


print('-' * 50)
print(v.shape)
s1 = np.vsplit(v, 3)
print(s1[0])
print(s1[1])
print(s1[2])



