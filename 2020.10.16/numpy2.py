import numpy as np

ran = np.random.default_rng(0)

a = (ran.random((5)) * 100).astype('int32')
b = (ran.random((5)) * 100).astype('int32')

print('a: ', a)
print('b: ', b)

r1 = np.where([True, True, False, False, True], a, b)
print(r1)

print(a > b)

print()
r2 = np.where(a > b, a, b)
print(r2)

print()

print((a > 50) & (a < 70))
r3 = np.where((a > 50) & (a < 70), a, b)
print(r3)

print('-' * 50)
c = (ran.random((10, 5)) * 100).astype('int32')
print(c)

d = np.argwhere(c == 99)
print(d)

d = np.argwhere(c >= 80)
print(d)

d = np.argwhere(c >= 50)
d = np.argwhere(d < 70)
print(d)

print('-' * 50)

print(c.argmax())
print(c.ravel()[c.argmax()])

print(c.argmin())
print(c.ravel()[c.argmin()])

