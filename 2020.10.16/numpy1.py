import numpy as np

ran = np.random.default_rng(0)

a = (ran.random((10)) * 100).astype('int32')
print(a)

# Slice
print(a[2:8])   #  [Desde , Hasta)

print(a[2:8:2])

print(a[::-1])

print(a[2::-1])

print('*' * 50)

b = np.arange(10) ** 3
print(b)

print(b ** (1/3))

print('*' * 50)

# f(x,y) = 5x + 2y

#                0     1
# 2,2       0    0     2
#           1    5     7

def f(x, y):
  return 5*x + 2*y

m = np.fromfunction(f, (5, 7)  , dtype=int)
print(m)

print(m.shape)
print(m.size)
# print(bytes(m.data))

for e in m.flat:
  print(e, end='\t')

print()

c = m.ravel()
print(c)

print('*' * 50)

m.resize(7,5)
print(m)

n = m.reshape(-1, 6)
print(n)