import numpy as np

ran = np.random.default_rng(0)

a = (ran.random((5, 6)) * 100)
print(a)

b = np.floor(a)
print(b)

c = np.ceil(a)
print(c)

d = a.T
print(d)


print('-' * 50)

x = (ran.random((3, 4)) * 100).astype('int32')
print(x)
print()
v = x.view()
print(v)
x[0,0] = -10
print(x)
print()
print(v)

print(x.data)
print(v.data)


print('-' * 50)

copia = x.copy()
x[0,0] = -20
print(x)
print(copia)
