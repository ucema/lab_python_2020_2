import numpy as np

vector1 = np.array([1,2,3,4])
print(vector1)

print(vector1[1])
print(type(vector1))

a = vector1 * 5
print(a)

vector2 = np.arange(50, 54, 1)
print(vector2)

b = vector1 + vector2
print(b)

print(vector2.ndim)

matriz1 = np.arange(0, 25, 1)
print(matriz1.size)

matriz2 = matriz1.reshape(5, 5)
print(matriz2)
print(matriz2.ndim)
print(matriz2.shape)

matriz3 = np.arange(0,36, 1)
matriz4 = matriz3.reshape(12, 3)
print(matriz4)
print(matriz4.ndim)
print(matriz4.shape)

print('*' * 50)

matriz5 = matriz4.reshape(4, 3, 3)
print(matriz5)
print(matriz5.ndim)
print(matriz5.shape)

print('*' * 50)

print(matriz5.dtype)

vector3 = np.array([2, 5, 6], dtype='complex')
print(vector3.dtype)
print(vector3[0]+vector3[1])

print('*' * 50)

vacia = np.empty((4, 3))
print(vacia)
print(vacia.dtype)

print('*' * 50)

ceros = np.zeros((4, 3), dtype='int')
print(ceros)

print('*' * 50)

ceros = np.zeros((10, ), dtype='int')
print(ceros)

print('*' * 50)

# Genera un array con 20 posiciones equisdistantes entre cota inf 1 y sup 10
vec5 = np.linspace(1, 10, 20)
print(vec5)
