import numpy as np

a = np.array([1, 2, 3, 4])
b = np.array([8, 1, 7, 10])

resta = a-b
print(resta)

m = a * b
print(m)

print('*' * 50)

ran = np.random.default_rng(1)
b = ran.random((4, 4))    # Reales entre 0 y 1
print(b)

print('*' * 50)

# Enteros entre 0 y 50
enteros = b * 50
print(enteros)
enteros = enteros.astype('int32')

print('*' * 50)
print(enteros)

print('Suma: ', enteros.sum())
print('Min: ', enteros.min())
print('Max: ', enteros.max())
print('Avg: ', np.average(enteros))

print('*' * 50)

suma_filas = enteros.sum(axis=1)    # 0- Columna, 1- Fila
print(suma_filas)

promedios_filas = np.average(enteros, axis=1)
print(promedios_filas)

print('Std: ', enteros.std())
print('Promedio:', enteros.mean())
print('Mediana: ', np.median(enteros))

print(enteros.transpose())
print(np.invert(enteros))

print('Determinante:', np.linalg.det(enteros))


print('*' * 50)

f_e = np.exp(a)     # e elevado a
print(f_e)

f_raiz = np.sqrt(a)
print(f_raiz)
