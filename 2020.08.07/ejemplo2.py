# Operadores relacionales
# >   ==    <   <=    >=    !=

numero1 = int(input('Numero: '))
numero2 = int(input('Numero: '))

print(numero1 >= numero2)


# Condicional, determinar segun la verdad o falsedad de una proposicion logica que hacer

# Determinar si el primero es multiplo del segundo.

if numero1 % numero2 == 0:
    print('Es multiplo')
    print('Si, es multiplo')
else:
    print('No es multiplo')

print('-' * 50)

# Quiero saber si es multiplo de 5,4,3,2
if numero1 % 5 == 0:
    print('Es multiplo de 5')
elif numero1 % 4 == 0:
    print('Es multiplo de 4')
elif numero1 % 3 == 0:
    print('Es multiplo de 3')
elif numero1 % 2 == 0:
    print('Es multiplo de 2')
else:
    print('No es multiplo')

print('fin.')