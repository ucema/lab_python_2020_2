# Texto: Alfanumericos, String.  Encerrados entre ''   o   ""
# Numerico: enteros: int  (Integer)
#           reales:  float (double)

# Entrada -->  Proceso --> Salida

print('Hola')   # Se muestra por consola la palabra Hola
print('Mundo')

# Variables. 
a = 432     # la variable a contiene un numero entero: 432
b = 'abc'   # la variable b contiene un texto (String): abc
c = 3.14

print('El valor de la variable a es: ', a)

# Operadores aritmeticos
# + sumar
# - restar
# * producto
# // Division entera (Cociente de la division entera)
# / Division real
# % Resto de la division entera o modulo

d = 14 / 3
print(d)

d = 17 % 4
print(d)


# Operadores Logicos: Verdadero o Falso (True, False)
# Boolean: True / False
# and  (Y, conjuncion)
# or   (O, disyuncion)
# not  (no, negacion)

p = True
q = False

r = p and q
print(r)

print(p or q)

x = '100'
print(x * 3)

print('-' * 50)

nombre = input('Cual es tu nombre: ')
print('Hola ', nombre)

altura =  int(input('Cual es tu altura (cm): '))
print('Tu ombligo esta a ', altura / 2)

# f(x) = x + 1
# g(x) = x * 5
# fog(x) = f(g(x)) = (x * 5) + 1

cotizacion_dolar = float(input('Cuando cotiza hoy: '))
print('Dolar a ', cotizacion_dolar)

a = int('654')
