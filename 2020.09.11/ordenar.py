class Persona():
    nombre = ''
    edad = 0

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def __str__(self):
        return self.nombre

    def __lt__(self, otro):
        return self.nombre < otro.nombre


def mostrar_lista(lista):
    for elem in lista:
        print(elem, end='\t')

    print()


a = [5, 56, 345, 23, 65]  # list, tuple, dict

a.append(4)
a.sort()

print(a)

ps = []
ps.append(Persona('Uno', 1))
ps.append(Persona('Dos', 2))
ps.append(Persona('Tres', 3))
ps.append(Persona('Cuatro', 4))

mostrar_lista(ps)
ps.sort(reverse=True)
mostrar_lista(ps)

ps.reverse()
mostrar_lista(ps)



