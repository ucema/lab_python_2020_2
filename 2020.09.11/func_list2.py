class Persona():
    nombre = ''
    edad = 0

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    def __str__(self):
        return self.nombre + '-' + str(self.edad) 

    def __lt__(self, otro):
        return self.nombre < otro.nombre


def mostrar_lista(lista):
    for elem in lista:
        print(elem, end='\t')

    print()

ps = []
ps.append(Persona('Uno', 15))
ps.append(Persona('Dos', 22))
ps.append(Persona('Tres', 34))
ps.append(Persona('Cuatro', 4))


personas_mayores = list(filter(lambda p : p.edad >= 18 and p.nombre > 'M', ps))
mostrar_lista(personas_mayores)