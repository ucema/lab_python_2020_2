import random
from functools import reduce


lados = [4, 5, 10, 2, 6]
print(lados)

superficies = []
for lado in lados:
    superficies.append(lado * lado)

print(superficies)

# Crear una lista a partir de otra lista, haciendo alguna operacion

# map -> a partir de una lista, aplica una funcion  a cada elemento.

def calcular_superficie(lado):
    return lado * lado

superficies_v2 = list(map(calcular_superficie, lados))
print(superficies_v2)


superficies_v3 = list(map(lambda x : x * x, lados))
print(superficies_v3)

# 4: 16
# 5: 25
# ...
# [(4, 16), (5, 25), ...]


# zip -> unificar 2 o mas colecciones
datos = list(zip(lados, superficies_v3))
print(datos)

a = [1,2,3]
b = [4,5,6]
c = [7,8,9]
n = list(zip(a,b,c))
print(n)

print('*' * 50)

random.seed(0)

numeros = []    # random.randint(desde, hasta)
for x in range(20):
    numeros.append(random.randrange(0, 100))

print(numeros)

mayores_50 = []
for n in numeros:
    if n > 50:
        mayores_50.append(n)

mayores_50_v2 = list(filter(lambda n : n > 50, numeros))
print(mayores_50_v2)

pares = list(filter(lambda n : n % 2 == 0, numeros))
print(pares)

print('*' * 50)

print(numeros)
print(sum(numeros))

suma_nueva = reduce(lambda resultado, elemento: resultado + elemento, numeros)
print(suma_nueva)

nombres = ['uno', 'dos', 'tres', 'cuatro']
todos = reduce(lambda res, ele: res + '-' + ele, nombres)
print(todos)
