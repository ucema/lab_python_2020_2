# f(x) = 2x+10

def construir_lambda(n):    # x^n
    l = lambda x : x ** n
    return l

f = lambda x : 2*x+10
g = lambda x,y : x*y

print(f(2))
print(g(3,4))

h2 = construir_lambda(2)    # h2(x) = x^2
h4 = construir_lambda(4)    # h4(x) = x^4

print(h2(3))
print(h4(3))
