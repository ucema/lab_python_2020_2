# Laboratorio programación Python


## Unidad 1: 
Instalación y primeros pasos. Instalación de Phyton 3. Manejo del IDE de desarrollo. Conceptos de variable, vector, matriz. Asignación dinámica. Tipo de objetos. Referencia múltiple. Listas, Tuplas, Diccionarios. Funciones. For & range. Variables globales vs locales. Recursividad. Uso de __main__.

## Unidad 2: 
Trabajos con Colecciones. Imprimir componentes. Longitud de vectores. Agregado y eliminación de componentes. Listas anidadas. Análisis de componentes. Diccionario. Crear modulo y paquete. Más sobre FOR y WHILE

## Unidad 3:
Introducción POO. La clase Object. Método __init__. Herencia múltiple. Uso de super. Métodos especiales, doble guion bajo al inicio y al final Uso de __all__. Variables y métodos de instancia, de clase, estáticos. Base de datos. Almacenamiento mediante POO.

## Unidad 4
Tópicos avanzados. Lambda (Anonymous) Functions. Try – Except y Exceptions. Archivos. Map(),Filter(), Reduce(), Zip(), Enumerate(),  All(), Any() Functions. Manipulando formato JSON. Slices.

## Unidad 5 
Manejo numérico. El paquete numpy. Cálculo numérico. Vectores. Matrices. Slicing.

## Unidad 6
Manipulación de datos. El paquete pandas. Datos relacionales/Etiquetados. Dataset. Dataframe. Estadísticas descriptivas. Ploting.


## Adicionales

Realizar peticiones web. 
Servidor web. 
Modulos en python

Base de datos.
Git. 
Persistencia de objetos. 
Jupyter Labs
Test Unitarios
IA

