# Peticion: GET
# REST: GET - Obtener un recurso
#       POST - Creacion de un recurso
#       DELETE - Eliminacion
#       PUT - Modificacion
#       ACTIONS - Ver acciones disponibles

# curl  https://curl.haxx.se/windows/

# Para pruebas: https://httpbin.org/

# www.google.com?q=python

# url friendly:    www.amazom.com/computacion/partes/discos/solidos


import requests, json

respuesta = requests.get('https://httpbin.org/get?valor1=1234&valor2=abc')

# respuesta es Response
# 1xx Informaciones
# 2xx Correcto
# 3xx Redireccionamientos
# 4xx: Errores de request
# 5xx: Error del servidor

if respuesta.status_code == 200:
    print('bien')
else:
    print(respuesta.status_code)


if respuesta.ok:
    print(respuesta.headers)
    print()

    data = respuesta.json()
    print(data['args'])

print('*' * 50)

# API Applicaction Programming Interface

resp_get = requests.get('https://swapi.dev/api/people/1')
if resp_get.ok:
    per = resp_get.json()
    print(per['name'])

    plan = requests.get(per['homeworld'])
    planeta = plan.json()
    print(planeta['name'])


