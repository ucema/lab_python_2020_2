class Felino():
    tipo_pelo = ''

    def __init__(self, tipo_pelo):
        print('Constructor Felino')
        self.tipo_pelo = tipo_pelo

    def ronronear(self):
        print('Rrrrrrr')


class Gato(Felino):
    edad = 0
    nombre = ''

    def __init__(self, nombre, edad=0, tipo_pelo='Corto'):
        super(Gato, self).__init__(tipo_pelo)
        self.nombre = nombre
        self.edad = edad
    
    def correr(self):
        print('Gato corriendo...')

    def dormir(self, horas):
        print('Durmiento', horas, 'horas.')

    def __str__(self):
        s = 'Soy ' + self.nombre
        s += ' y tengo ' + str(self.edad)
        return s


g = Gato('pipi', 10)
print(g.tipo_pelo)