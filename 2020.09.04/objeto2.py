# Todas las clases, heredan de una superclase Object

class Animal():
    peso = 0

    def comer(self):
        print('comiendo')
        self.peso = self.peso + 1


class Felino(Animal):
    tipo_pelo = ''

    def ronronear(self):
        print('Rrrrrrr')


class Gato(Felino):
    edad = 0
    nombre = ''

    def __init__(self, nombre, edad=0):
        self.nombre = nombre
        self.edad = edad
    
    def correr(self):
        print('Gato corriendo...')

    def dormir(self, horas):
        print('Durmiento', horas, 'horas.')

    def __str__(self):
        s = 'Soy ' + self.nombre
        s += ' y tengo ' + str(self.edad)
        return s


a = Gato('Aa', 10)
print(a)
a.ronronear()
a.comer()

b = Gato('Bb', 10)
print(b)

