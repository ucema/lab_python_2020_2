'''
DNI = 98
CUIT = 96
CUIL = 95
PASAPORTE = 10
'''

# enumerados
from enum import Enum     # desde el paquete enum importar la clase Enum

class TipoDocumento(Enum):
    DNI = 98
    CUIT = 96
    CUIL = 95
    PASAPORTE = 10

    def __str__(self):
        return 'Tipo documento ' + self.name + ' codigo ' + str(self.value)


print(TipoDocumento(98))
print(TipoDocumento.DNI)

print('*' * 50)

for td in TipoDocumento:
    print(td)

a = TipoDocumento.DNI
b = TipoDocumento.DNI

print(a == b)
print(a != b)
print(a is b)
print(a is not b)

print('*' * 50)
c = input('Tipo docu:')
d = TipoDocumento(c)
print(d)