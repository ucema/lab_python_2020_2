class Articulo():
    codigo = ''
    descripcion = ''
    precio = 0

    def __init__(self, codigo='', descripcion='', precio=0):
        self.codigo = codigo
        self.descripcion = descripcion
        self.precio = precio

    def __str__(self):
        return 'Codigo: {0}\tDescripcion: {1}\tPrecio: {2}'.format(self.codigo, self.descripcion, self.precio)

a = Articulo('Uno', 'Desc. Uno', 1.1)


print(a)
a.precio = 3.1
# print(a)

print('*' * 50)

atr = input('Atributo a modificar: ')
valor = input('Valor a poner: ')
a.__setattr__(atr, valor)
print(a)

print('*' * 50)

atr = input('Atributo a consultar: ')
valor = a.__getattribute__(atr)
print(valor)

print('*' * 50)

print(type(a).__name__)

print('*' * 50)

a.descuento = 10          # Solo se agrega al objeto.
print(a.descuento)

# b = Articulo('Dos', 'Desc. Dos', 2.2)
# print(b.descuento)          # Da error pq b no tiene descuento