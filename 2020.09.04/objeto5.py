# Herencia multiple

class ClaseA():
    def __init__(self):
        print('Constructor A')

class ClaseB():
    def __init__(self):
        print('Constructor B')

class ClaseC(ClaseA, ClaseB):
    def __init__(self):
        super(ClaseC, self).__init__()
        print('Constructor C')

    
c = ClaseC()
