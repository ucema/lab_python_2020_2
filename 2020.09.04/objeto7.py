class Persona():
    nombre = ''
    edad = 0
    altura = 0

    def __init__(self, nombre, edad, altura):
        self.nombre = nombre
        self.edad = edad
        self.altura = altura

    def __str__(self):
        return 'Soy {0}, tengo {1} y mido {2}'.format(self.nombre, self.edad, self.altura)

    def __eq__(self, otro): # equals, devuelve True o False
        if isinstance(otro, Persona):
            return self.nombre == otro.nombre

        return False

    def __ne__(self, otro):
        if isinstance(otro, Persona):
            return self.nombre != otro.nombre

        return False

    def __lt__(self, otro):
        return self.edad < otro.edad

    def __gt__(self, otro):
        return self.edad > otro.edad

    def __le__(self, otro):
        return self.edad <= otro.edad

    def __ge__(self, otro):
        return self.edad >= otro.edad


a = Persona('Pepe', 30, 170)
print(a)

b = Persona('Pepe', 34, 170)
print(b)

print(a == b)       # a.__eq__(b)

print(a == 'Pepe')

print(a < b)
print(b > a)