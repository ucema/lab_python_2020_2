# Herencia multiple

class ClaseA():
    def metodo1(self):
        print('ClaseA.metodo1()')

class ClaseB(ClaseA):
    def metodo1(self):
        print('ClaseB.metodo1()')

class ClaseC(ClaseA):
    def metodo1(self):
        print('ClaseC.metodo1()')

class ClaseD(ClaseC, ClaseB):
    pass
    
a = ClaseA()
a.metodo1()

print('*' * 50)

b = ClaseB()
b.metodo1()

print('*' * 50)
d = ClaseD()
d.metodo1()