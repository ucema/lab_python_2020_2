# Paradigmas. Objetos.
'''
Proceso de abstraccion
1. Carateristicas

2. Comportamiento

Gato: color_pelo, edad, correr, dormir

CLASE: Es un "especie" de plano (codificado) para crear los objetos en memoria
'''

class Gato():
    # Atributos (de la clase) --> Las caracteristicas de los objetos
    color_pelo = ''
    edad = 0
    nombre = ''

    # Metodos (de la clase) --> El comportamiento de los objetos
    def correr(self):
        print('Gato corriendo...')

    def dormir(self, horas):
        print('Durmiento', horas, 'horas.')


class Cuidador():
    nombre = ''         # Variables de estado del objeto
    gatos = []

    # Metodo constructor, en todas las clases.
    # Metodo constructor por default -> No Hace NADA
    def __init__(self, nombre):     # Sobre escritura, cambiar el comportamiento de un metodo
        self.nombre = nombre

    def cuidar(self, gato):
        self.gatos.append(gato)

    def correr_gatos(self):
        print('Soy', self.nombre)
        for gato in self.gatos:
            gato.correr()


cuidador = Cuidador('Brutus')

pipi = Gato()
pepe = Gato()

cuidador.cuidar(pipi)
cuidador.cuidar(pepe)

cuidador.correr_gatos()