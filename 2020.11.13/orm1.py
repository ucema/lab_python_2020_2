# SqlServer, MySQl, MariaDb, Postgres, Oracle, Sybase, Sqlite, Access, etc.
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

Base = declarative_base()

class Producto(Base):
    __tablename__ = 'productos'
    id = Column(Integer, primary_key=True)
    descripcion = Column(String)
    precio = Column(Float)
    peso = Column(Float)
    familia = Column(String)

    def __str__(self):
        return '{0}-{1}'.format(self.id, self.descripcion)


class Familia(Base):
    __tablename__ = 'familias'
    id = Column(Integer, primary_key=True)
    descripcion = Column(String)

    def __str__(self):
        return '{0}-{1}'.format(self.id, self.descripcion)


engine = create_engine('sqlite:///mibase.db')    # String de conexion

Base.metadata.create_all(engine)

Session = sessionmaker()
Session.configure(bind=engine)


a = Producto()
a.descripcion = 'Uno'
a.precio = 1.1
a.peso = 10
a.familia = 'Fa1'

b = Producto()
b.descripcion = 'Dos'
b.precio = 2.1
b.peso = 20
b.familia = 'Fa1'


# s = Session()       # Transaccion
# s.add(a)
# s.add(b)
# s.commit()

# print(a.id)

s = Session()

prods = s.query(Producto)
for p in prods:
    print(p)

print('*' * 50)

producto3 = s.query(Producto).filter(Producto.descripcion=='Dos')
for p in producto3:
    print(p)
