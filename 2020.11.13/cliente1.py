import requests, json

# import urllib.parse
# 

class ClienteProducto():

    def __init__(self, url):
        self.url = url

    def listar_productos(self):
        resp = requests.get(self.url)

        if resp.ok:
            datos = resp.json()
            return datos
        else:
            raise Exception(str(resp.status_code) + '(listar_productos)')

    def crear_producto(self, id, nombre, familia):
        datos = {
            'id': id, 
            'nombre': nombre,
            'familia': familia
        }

        resp = requests.post(self.url, data=datos)
        if not resp.ok:
            raise Exception(str(resp.status_code) + '(crear_producto)')
        
    def leer_producto(self, familia, id):
        
        resp = requests.get(self.url + familia + '/' + id)
        if resp.ok:
            datos = resp.text
            return datos
        else:
            raise Exception(str(resp.status_code) + '(leer_producto)')


cliente = ClienteProducto('http://localhost:5000/productos/')

try:
    productos = cliente.listar_productos()
    for fam in productos:
        print('Familia: ', fam)
        for pro in productos[fam]:
            print('\t', pro, productos[fam][pro])

    cliente.crear_producto(20, 'sandia', 'frutas')
    cliente.crear_producto(30, 'el complicado', 'los complicados')
    cliente.crear_producto(31, 'día', 'los complicados')

    p = cliente.leer_producto('los complicados', '30')
    print(p)


except Exception as e:
    print('Error: ', e)


