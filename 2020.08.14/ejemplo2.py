i = 0

while i < 5:
    print('Hola', i)

    i = i + 1

print('-' * 50) 

# Se realiza una encuesta, se cargan datos mientras la EDAD sea mayor a cero,
# se pide la EDAD y el NOMBRE. Al finalizar mostrar el nombre del mayor.

# Inicializar variables de la condicion
edad = int(input('Ingrese edad: '))
edad_mayor = edad

while edad > 0:
    # Pedir resto de datos
    nombre = input('Ingrese nombre: ')

    # Procesar
    if edad >= edad_mayor:
        edad_mayor = edad
        nombre_mayor = nombre

    # Actualizar variables de la condicion
    edad = int(input('Ingrese edad: '))
    
print('La persona con mayor edad es', nombre_mayor, 'con', edad_mayor)
