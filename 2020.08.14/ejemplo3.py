# Lista ( list )

lista1 = [2, 6, 7, 10, 5, 3, 2, 14]
lista2 = ['a', 'b', 'c', 'd', 'e']
lista3 = [True, False, True, True, False, False]

print('Primer elemento', lista1[0])
print('Segundo elemento', lista1[1])

n = len(lista1)
print('Cantidad', n)

i = 0
while i < len(lista1):
    print(i, lista1[i])
    i = i + 1

print('-' * 50)

print(lista1)
lista1[3] = 15
print(lista1)

print('-' * 50)

lista1.append(100)
lista1.append(200)
print(lista1)

print('-' * 50)

lista1.extend([300, 400, 500])

print(lista1)

print('-' * 50)

lista1.insert(3, 600)

print(lista1)

print('-' * 50)
print('SACAR ELEMENTOS')

n = lista1.pop(3)
print(lista1)
print(n)

print('-' * 50)

lista1.remove(100)
print(lista1)

lista1.remove(2)
print(lista1)

print('-' * 50)
# n = lista1.pop(30)    # IndexError: pop index out of range
# print(n)

if 22222 in lista1:
    lista1.remove(22222)    #  list.remove(x): x not in list
    print(lista1)
else:
    print('No estaba')

print('-' * 50)

lista1.reverse()
print(lista1)

print('-' * 50)

lista1.sort()
print(lista1)

print('-' * 50)

lista1.sort(reverse=True)
print(lista1)

