# slice

lista = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
print(lista)

a = lista[4]
print('lista[4] = \t', a)

b = lista[7:]     # Desde el indice 7 al final
print('lista[7:] = \t', b)

c = lista[3:6]     # Desde el indice 3 hasta 6-1 (o sea 5)
print('lista[3:6] = \t',c)

d = lista[:4]     # equivalente a lista[0:4]
print('lista[:4] = \t', d)

e = lista[1:9:2]
print('lista[1:9:2] = \t', e)

f = lista[::2]     # equivalente a lista[0:len(lista):2]
print('lista[::2] = \t', f)

g = lista[::-1]     # equivalente a lista[len(lista):0:-1]
print('lista[::-1] = \t', g)
