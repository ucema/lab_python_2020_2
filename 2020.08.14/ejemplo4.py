
animales = ['gato', 'leon', 'perro', 'delfin', 'cotorra', 'orca', 'lombriz']

for animal in animales:     # para cada animal en animales hacer
    print(animal)


numeros = [20, 5, 69, 45, 34, 14]
suma_numeros = 0
for n in numeros:
    print('Voy a sumar el numero', n)
    suma_numeros = suma_numeros + n
    print()

print(suma_numeros)


s = sum(numeros)
print(s)
