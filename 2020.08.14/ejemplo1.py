
a = 10.123
print('A vale ' + str(a))    # + es concatenar (unir texto)

p = True
print('P vale ' + str(p))

q = 'True'
print(p and bool(q))        # bool -> boolean valores logico: True / False

print(type(p))
print(type(q))

print('-' * 50)

if 1432 and -150:            # El 0 (Cero) equivale a FALSE, dinstinto de Cero es TRUE
    print('SI')
else:
    print('NO')

print('-' * 50)


a = int(input('Numero a: '))
b = int(input('Numero b: '))
c = int(input('Numero c: '))

if a > b and a > c:
    print(a)
elif b > a and b > c:
    print(b)
elif c > a and c > b:
    print(c)

