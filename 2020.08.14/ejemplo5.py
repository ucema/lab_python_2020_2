
numeros = [20, 5, 69, 45, 34, 14]
print(type(numeros), numeros)

r1 = list(range(10))  # 0,1,2,3,4,5,6,7,8,9
print(type(r1), r1)

print('-' * 50)

for n in range(10):   # iterable
    print(n)

print('-' * 50)

for n in range(4, 10):   # [4, 10)
    print(n)

print('-' * 50)

for n in range(4, 20, 3):   # el 3 es el step o salto
    print(n)

print('-' * 50)

for n in range(2, 20, 2):   # el 3 es el step o salto
    print(n)

print('-' * 50)

for n in range(10, 0, -1):   # el 3 es el step o salto
    print(n)

