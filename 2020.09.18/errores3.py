class ContabilidadError(Exception):
    pass

class AsientoDesbalanceadoError(ContabilidadError):
    pass

class CuentaContableInvalidadError(ContabilidadError):
    pass

