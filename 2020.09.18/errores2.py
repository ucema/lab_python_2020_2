class MiError(Exception):
    
    def __init__(self, numero_ingreso, leyenda='Mi Error :('):
        super(MiError, self).__init__(leyenda)
        self.numero_ingreso = numero_ingreso


def leer():
    n = int(input('Ingrese numero:'))
    if n < 0:
        raise MiError(n, 'Numero no valido')

    return n

try:
    x = leer()
    print(x)

except MiError as m:
    print('Error')
    print(m)
    print(m.numero_ingreso)
    
