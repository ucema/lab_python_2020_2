# modo del archivo: 
# w: Escritura
# r: Lectura
# a: Agregar (append)
# x: Escritura (si el archivo no existe, sino error)

# t: texto (default)
# b: binario

arch1 = open('archivo1.txt', 'w')
arch1.write('linea uno\n')
arch1.write('linea dos\n')
arch1.write('linea tres\n')
arch1.close()


arch2 = open('archivo1.txt', 'r')
contenido = arch2.read()
print(contenido)
arch2.close()

print('*' * 50)

arch2 = open('archivo1.txt', 'r')
for linea in arch2:
    print(linea, end='')
arch2.close()

print('*' * 50)

arch2 = open('archivo1.txt', 'r')
lineas = arch2.readlines()
print(lineas)
arch2.close()

print('*' * 50)

arch2 = open('archivo1.txt', 'r')
linea = arch2.readline(15)
print(linea)
arch2.close()

print('*' * 50)

arch1 = open('archivo1.txt', 'a')
arch1.write('xxxxx linea uno\n')
arch1.write('xxxxx linea dos\n')
arch1.write('xxxxx linea tres\n')
arch1.close()

arch2 = open('archivo1.txt', 'r')
contenido = arch2.read()
print(contenido)
arch2.close()

print('*' * 50)

# arch1 = open('archivo2.txt', 'x')
# arch1.write('gggggg linea uno\n')
# arch1.write('gggggg linea dos\n')
# arch1.close()

arch2 = open('archivo1.txt')    # default: 'rt'
contenido = arch2.read()
print(contenido)
arch2.close()