# Excepciones

try:
    # El codigo potencialmente q pueda producir error
    x = 0
    print(19 / x)

    arch1 = open('archivo2.txt', 'x')
    arch1.write('gggggg linea uno\n')
    arch1.write('gggggg linea dos\n')
    arch1.close()

except ZeroDivisionError as z:
    print('ZeroDivisionError')

except ArithmeticError as a:
    print('ArithmeticError', a)

except FileExistsError as f:
    print('FileExistsError')

except Exception as e:
    # Que se hace cuando hay error
    print('Exception')


print('Fin feliz')