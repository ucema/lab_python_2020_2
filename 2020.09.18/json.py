# JSON: Javascript Object Notation
# HTTP / HTTPS
import json

'''
(Navegador) -----> Hace peticion ----> (Servidor)
 Javascript           (texto)            Python

 (Navegador) <----- Respuesta   <---- (Servidor)
                     (texto)
'''

t = '''
{
  "nombre": "Gabriel",
  "apellido": "Barrera",
  "peliculas_fav": ["Peli1", "Peli2"]
}
'''
o = json.loads(t)
print(o)
print(o['nombre'])
print(o['peliculas_fav'])


class Persona():
    nombre = ''
    edad = 0
    altura = 0

p = Persona()
p.nombre = 'Pipi'
p.edad = 20
p.altura = 168

print(p.__dict__)

r = json.dumps(p.__dict__)
print('En JSON es: ', r)
print(type(r))