def ejemplo(**diccionario):
    for d in diccionario:
        print(d, diccionario[d])


def super_ejemplo(param_pos1, param_pos2, param_def1=10, param_def2=20, *argv, **kwargs):
    print('Super completo')
    print(param_pos1)
    print(param_pos2)
    print(param_def1)
    print(param_def2)
    print(argv)
    print(kwargs)


ejemplo(param=123, param2='abc', param3=456, param4='cccc')

ejemplo(param=123, param2='abc', param3=456)

super_ejemplo(1, 2, 44, 'a', 'b', 'c', nn1=123, nn2=345)
