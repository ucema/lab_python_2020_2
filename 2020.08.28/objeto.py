# Paradigmas. Objetos.
'''
Proceso de abstraccion
1. Carateristicas

2. Comportamiento

Gato: color_pelo, edad, correr, dormir

CLASE: Es un "especie" de plano (codificado) para crear los objetos en memoria
'''

class Gato():
    # Atributos (de la clase) --> Las caracteristicas de los objetos
    color_pelo = ''
    edad = 0

    # Metodos (de la clase) --> El comportamiento de los objetos
    def correr(self):
        print('Gato corriendo...')

    def dormir(self, horas):
        print('Durmiento', horas, 'horas.')

# Creacion de un objeto
pipi = Gato()
pepe = Gato()

pipi.correr()
pepe.correr()
