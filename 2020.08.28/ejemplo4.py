# f(x) = 2x+2
# f(2) --> 6

def f(x):
    resultado = 2 * x + 2
    return resultado

def sumar(lista):
    total = 0
    for numero in lista:
        total = total + numero

    return total


def sumar_multiplos(lista, divisor):
    total = 0
    # print(no_me_deben_usar)
    for numero in lista:
        if numero % divisor == 0:
            total = total + numero

    return total


def leer_numero_positivo():
    nro = int(input('Ingrese un numero:'))
    while nro <= 0:
        nro = int(input('Ingrese un numero:'))

    return nro

y = f(2)
print(y)

a = [5,8,3,45,12]
s = sumar(a)
print(s)

b = leer_numero_positivo()
print(b)

# no_me_deben_usar = 'Soy una mala variable'
c = sumar_multiplos(a, 2)
print(c)