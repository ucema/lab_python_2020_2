# Diccionarios: Key (Clave) - Value (Valor)
'''
Key       Value
1         Auto
2         Auto
3         Moto
'''

dic = {
  'k1': 'uno',
  'k2': 'dos',
  'k3': [10, 20, 30]
}

v = dic['k2']
print(v)

v = dic['k3']
print(v[1])

dic['k4'] = 'valor agregado'
print(dic)

print('*' * 50)

dic2 = {}
dic2['uno'] = 'este es el valor?'
dic2['dos'] = 'otro valor'
print(dic2)

print('*' * 50)

for x in dic:
    print(x, dic[x])

print('*' * 50)

for x in dic.items():
    print(x)

print('*' * 50)

v = dic.pop('k2') 
print(dic)
print('Se sacó:', v)

print('*' * 50)

v = dic.popitem()

print(dic)
print('Se sacó:', v)

print('*' * 50)

l = len(dic)
print('Long de dic:', l)
