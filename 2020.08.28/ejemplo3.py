# Tuplas

t1 = ('uno', 10, [1,2,3,4])
print(t1)

print(t1[0])

print(len(t1))

print('*' * 50)

for x in t1:
    print(x)

print('*' * 50)

# t1[1] = 20   # Da error

l1 = list(t1)
print(l1, t1)
l1[1] = 20
print(l1, t1)
t1 = tuple(l1)
print(t1)
