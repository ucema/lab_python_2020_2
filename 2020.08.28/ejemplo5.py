def sumar_multiplos(lista, divisor=2):
    total = 0

    for numero in lista:
        if numero % divisor == 0:
            total = total + numero

    return total

def ejemplo(param1, param2=123, param3=456):
    print('-' * 50)
    print(param1)
    print(param2)
    print(param3)
    print('-' * 50)

numeros = [20, 5, 69, 45, 34, 14]
s = sumar_multiplos(numeros)
print(s)

s = sumar_multiplos(numeros, 3)
print(s)

s = sumar_multiplos(numeros, 5)
print(s)

ejemplo(421, param3=5555)
